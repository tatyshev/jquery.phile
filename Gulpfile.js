var gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    header = require('gulp-header');

var pkg = require('./package.json');

var bunner = [
  '/*!',
  ' * jQuery.Phile.js <%= pkg.version %>',
  ' * by <%= pkg.author %>',
  ' * <%= pkg.homepage %>',
  ' */\n'
];

gulp.task('default', function(){
  gulp.start('jshint');
  gulp.start('build');
});

gulp.task('jshint', function(){
  gulp.src('./src/*.js')
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'))
});

gulp.task('build', function(){
  gulp.src('./src/*.js')
      .pipe(header( bunner.join('\n'), {pkg: pkg} ))
      .pipe(gulp.dest('./dist'));

  gulp.src('./src/*.js')
      .pipe(uglify())
      .pipe(header( bunner.join('\n'), {pkg: pkg} ))
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest('./dist'));
});

gulp.task('watch', function(){
  gulp.watch('./src/*.js', ['default']);
});