/*!
 * jQuery.Phile.js 1.0.2
 * by Ruslan Tatyshev <@ruslanx32>
 * https://github.com/ruslanx32/jQuery.Phile
 */
!(function($){
  'use strict';

  var defaults = {
    wrapClass: 'phile',
    textClass: 'text',
    buttonClass: 'button',
    buttonText: 'Browse',
    placeholder: '',
    reversed: false
  };

  function Phile ( element, options ) {
    this.element = $(element);
    this.settings = $.extend({}, defaults, options);
    this.init();
  }

  $.extend(Phile.prototype, {
    init: function(){
      if( ! this.isFile() ) {
        return;
      }

      // aliases for this props
      var settings = this.settings,
          element = this.element;

      // Additional elements
      var text = this.text   = $('<input type="text" readonly>'),
          button = this.button = $('<input type="button">'),
          label = this.label  = element.wrap('<label></label>').parent();

      // Prepare elements
      label
        .addClass( element.attr('class') )
        .addClass( settings.wrapClass );

      if(settings.reversed) {
        label.append(button, text);
      } else {
        label.append(text, button);
      }

      element
        .css({display: 'none'})
        .on('change', function(){
          text.val(this.value.replace('C:\\fakepath\\', ''));
        });

      text
        .addClass( settings.textClass )
        .attr({
          placeholder: element.attr('placeholder') || settings.placeholder
        })
        .on('click', function(){
          element.get(0).dispatchEvent(clickEvent);
        });

      button
        .addClass( settings.buttonClass )
        .val( settings.buttonText )
        .on('click', function(){
          element.trigger('click');
        });

      if((/safari/i).test(navigator.userAgent)) {
        this.useOverlay();
      }
    },

    useOverlay: function() {
      this.label.css({
        position: 'relative'
      });

      this.element.css({
        display: 'block',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        'z-index': 1,
        'opacity': 0
      });
    },

    isFile: function(){
      var tag = this.element.prop('tagName').toLowerCase(),
          type = this.element.attr('type').toLowerCase();

      return tag === 'input' && type === 'file';
    }
  });

  $.fn.phile = function( options ){
    return this.each(function(){
      if( !$.data(this, 'phile') ) {
        $.data(this, 'phile', new Phile(this, options));
      }
    });
  };

})(jQuery);