# jQuery.Phile [Demo](http://htmlpreview.github.io/?https://raw.githubusercontent.com/ruslanx32/jQuery.Phile/master/demo/remote.html)
Make file inputs styleable and more friendly for css

## Usage

1. Include jQuery:

  ```html
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  ```

2. Include plugin's code:

  ```html
  <script src='dist/jquery.phile.min.js'></script>
  ```

3. Call the plugin:

  ```javascript
  $("input[type='file']").phile();
  ```

4. Add some css:

  ```css
  .phile {
    ...
  }

  .phile .text {
    ...
  }

  .phile .button {
    ...
  }
  ```

## Structure

The basic structure of the project is given in the following way:

```
├── demo/
│   └── index.html
├── dist/
│   ├── jquery.phile.js
│   └── jquery.phile.min.js
├── src/
│   └── jquery.phile.js
├── .gitignore
├── .jshintrc
├── Gulpfile.js
├── README.md
└── package.json
```

## Options

#### wrapClass [default: "phile"]
Root wrapper class name. `$("input").phile({ wrapClass: "customWrapClass" })`

#### textClass [default: "text"]
Class name for fallback input text element. `$("input").phile({ textClass: "customTextClass" })`

#### buttonClass [default: "button"]
Class name for button. `$("input").phile({ buttonClass: "customButtonClass" })`

#### buttonText [default: "Browse"]
Custom text for button. `$("input").phile({ buttonText: "chooseFile" })`

#### placeholder [default: ""]
Custom placeholder for input text fallback: `$("input").phile({ placeholder: "Choose file..." })`
> *Also, you can specify the [placeholder] attribute for input[file] it will be used as default values automatically. `<input type="file" placeholder="Choose file...">`*

#### reversed [default: false]
Swap order of text and button. `$("input").phile({ reversed: true })`

## History

Check [Releases](https://github.com/ruslanx32/jQuery.Phile/releases) for detailed changelog.

## License

[MIT License](http://zenorocha.mit-license.org/) © Zeno Rocha
